---
- hosts: localhost
  tasks:
    - name: Creating build directory
      file:
        path: ./build
        state: directory

    - name: Creating Docker build files
      template: src={{ docker_template }}/{{ item.name }}.j2 dest=./build/{{ item.name }}
      with_items:
        - { name: Dockerfile.base}
        - { name: Dockerfile.common}
        - { name: Dockerfile.api}
        - { name: Dockerfile.conductor}
        - { name: Dockerfile.replica_cron}
        - { name: Dockerfile.worker}
        - { name: Dockerfile.web}
        - { name: Dockerfile.webproxy}
        - { name: webproxy_start}

    - name: Creating coriolis-web virtualhost config
      template:
        src: ./{{ docker_template }}/coriolis-web-vhost.conf.j2
        dest: ./build/coriolis-web-vhost.conf

    - name: Cloning coriolis git repositories
      git: repo={{ item.url }} dest=./build/{{item.name}} update=yes force=yes version={{item.version}}
      with_items:
        - { url: "https://github.com/cloudbase/coriolis", name: coriolis-core, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-oracle-vm.git", name: coriolis-provider-oracle-vm, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-opc.git", name: coriolis-provider-opc, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-oci.git", name: coriolis-provider-oci, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-vmware.git", name: coriolis-provider-vmware, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-aws.git", name: coriolis-provider-aws, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-openstack.git", name: coriolis-provider-openstack, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-azure.git", name: coriolis-provider-azure, version: master}
        - { url: "git@bitbucket.org:cloudbase/coriolis-provider-scvmm.git", name: coriolis-provider-scvmm, version: master}
        - { url: "https://github.com/cloudbase/coriolis-web", name: coriolis-web, version: master}

    - name: Downloading VMware disklib
      get_url:
        url: "{{ vmware_disklib_url }}"
        dest: ./build/VMware-vix-disklib.tar.gz

    - name: Downloading libnfs RPM
      get_url:
        url: "{{ libnfs_rpm_url }}"
        dest: ./build/libnfs.x86_64.rpm

    - name: Downloading libqemu
      get_url:
        url: "{{ libqemu_url }}"
        dest: ./build/libqemu.so.gz

    - name: Building coriolis-base
      docker_image:
        path: ./build
        dockerfile: Dockerfile.base
        tag: "{{ docker_images_tag }}"
        push: "{{ docker_push_images}}"
        name: "{{ docker_registry }}/{{ coriolis_containers_namespace }}/coriolis-base"

    - name: Building coriolis docker images
      docker_image: path=./build dockerfile={{item.dockerfile}} name={{ docker_registry }}/{{ coriolis_containers_namespace }}/{{item.name}} pull=False push={{ docker_push_images}} tag={{ docker_images_tag }}
      with_items:
        - { dockerfile: Dockerfile.common, name: coriolis-common}
        - { dockerfile: Dockerfile.api, name: coriolis-api}
        - { dockerfile: Dockerfile.conductor, name: coriolis-conductor}
        - { dockerfile: Dockerfile.replica_cron, name: coriolis-replica-cron}
        - { dockerfile: Dockerfile.worker, name: coriolis-worker}
        - { dockerfile: Dockerfile.web, name: coriolis-web}
        - { dockerfile: Dockerfile.webproxy, name: coriolis-web-proxy}
