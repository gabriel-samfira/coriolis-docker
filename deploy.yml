---
- hosts: localhost

  tasks:
    - name: Creating coriolis database
      mysql_db:
        login_host: "{{ database_address }}"
        login_port: "{{ database_port }}"
        login_user: "{{ database_user }}"
        login_password: "{{ database_password }}"
        name: "{{ coriolis_database_name }}"
      run_once: True

    - name: Creating coriolis database user and setting permissions
      mysql_user:
        login_host: "{{ database_address }}"
        login_port: "{{ database_port }}"
        login_user: "{{ database_user }}"
        login_password: "{{ database_password }}"
        name: "{{ coriolis_database_name }}"
        password: "{{ coriolis_database_password }}"
        host: "%"
        priv: "{{ coriolis_database_name }}.*:ALL"
        append_privs: "yes"
      run_once: True

    - name: Creating config directory
      file:
        path: "{{ coriolis_config_directory_path }}"
        state: directory

    - name: Creating log directory
      file:
        path: /var/log/coriolis
        state: directory

    - name: Creating coriolis.conf
      template:
        src: ./templates/coriolis.conf.j2
        dest: "{{ coriolis_config_directory_path }}/coriolis.conf"
        force: no

    - name: Creating api-paste.ini
      template:
        src: ./templates/api-paste.ini.j2
        dest: "{{ coriolis_config_directory_path }}/api-paste.ini"

    - name: Creating oslo.policy file
      template:
        src: ./templates/policy.yml.j2
        dest: "{{ coriolis_policy_config_path }}"

    - name: Checking Coriolis Keystone service
      shell: openstack service list -c Name | grep coriolis
      environment:
        OS_AUTH_URL: "{{ keystone_auth_url }}"
        OS_IDENTITY_API_VERSION: 3
        OS_PASSWORD: "{{ keystone_admin_password }}"
        OS_PROJECT_NAME: admin
        OS_TENANT_NAME: admin
        OS_USERNAME: admin
        OS_USER_DOMAIN_NAME: "default"
        OS_PROJECT_DOMAIN_NAME: "default"
      retries: 10
      delay: 5
      register: service_list
      failed_when: "service_list.rc > 1"

    - name: Creating Coriolis Keystone service
      command: openstack service create --name coriolis --description "Cloud Migration as a Service" migration
      environment:
        OS_AUTH_URL: "{{ keystone_auth_url }}"
        OS_IDENTITY_API_VERSION: 3
        OS_PASSWORD: "{{ keystone_admin_password }}"
        OS_PROJECT_NAME: admin
        OS_TENANT_NAME: admin
        OS_USERNAME: admin
        OS_USER_DOMAIN_NAME: "default"
        OS_PROJECT_DOMAIN_NAME: "default"
      run_once: True
      retries: 10
      delay: 5
      when: service_list.rc == 1

    - name: Checking Coriolis Keystone endpoints
      shell: openstack endpoint list -c "Service Name" | grep coriolis
      environment:
        OS_AUTH_URL: "{{ keystone_auth_url }}"
        OS_IDENTITY_API_VERSION: 3
        OS_PASSWORD: "{{ keystone_admin_password }}"
        OS_PROJECT_NAME: admin
        OS_TENANT_NAME: admin
        OS_USERNAME: admin
        OS_USER_DOMAIN_NAME: "default"
        OS_PROJECT_DOMAIN_NAME: "default"
      retries: 10
      delay: 5
      register: endpoint_list
      failed_when: "endpoint_list.rc > 1"

    - name: Creating Coriolis Keystone endpoints
      command: "openstack endpoint create migration {{ item.interface }} {{ item.url }}"
      environment:
        OS_AUTH_URL: "{{ keystone_auth_url }}"
        OS_IDENTITY_API_VERSION: 3
        OS_PASSWORD: "{{ keystone_admin_password }}"
        OS_PROJECT_NAME: admin
        OS_TENANT_NAME: admin
        OS_USERNAME: admin
        OS_USER_DOMAIN_NAME: "default"
        OS_PROJECT_DOMAIN_NAME: "default"
      run_once: True
      retries: 10
      delay: 5
      when: endpoint_list.rc == 1
      with_items:
        - {'interface': 'admin', 'url': '{{ coriolis_admin_endpoint }}'}
        - {'interface': 'internal', 'url': '{{ coriolis_internal_endpoint }}'}
        - {'interface': 'public', 'url': '{{ coriolis_public_endpoint }}'}

    - name: Checking Coriolis Keystone user
      shell: openstack user list -c Name | grep {{ coriolis_keystone_user }}
      environment:
        OS_AUTH_URL: "{{ keystone_auth_url }}"
        OS_IDENTITY_API_VERSION: 3
        OS_PASSWORD: "{{ keystone_admin_password }}"
        OS_PROJECT_NAME: admin
        OS_TENANT_NAME: admin
        OS_USERNAME: admin
        OS_USER_DOMAIN_NAME: "default"
        OS_PROJECT_DOMAIN_NAME: "default"
      retries: 10
      delay: 5
      register: user_list
      failed_when: "user_list.rc > 1"

    - name: Creating Coriolis Keystone user
      command: "openstack user create --domain default --password '{{ coriolis_keystone_password }}' {{ coriolis_keystone_user }}"
      environment:
        OS_AUTH_URL: "{{ keystone_auth_url }}"
        OS_IDENTITY_API_VERSION: 3
        OS_PASSWORD: "{{ keystone_admin_password }}"
        OS_PROJECT_NAME: admin
        OS_TENANT_NAME: admin
        OS_USERNAME: admin
        OS_USER_DOMAIN_NAME: "default"
        OS_PROJECT_DOMAIN_NAME: "default"
      run_once: True
      retries: 10
      delay: 5
      when: user_list.rc == 1

    - name: Adding Coriolis Keystone user to the service admin role
      command: "openstack role add --project service --user {{ coriolis_keystone_user }} admin"
      environment:
        OS_AUTH_URL: "{{ keystone_auth_url }}"
        OS_IDENTITY_API_VERSION: 3
        OS_PASSWORD: "{{ keystone_admin_password }}"
        OS_PROJECT_NAME: admin
        OS_TENANT_NAME: admin
        OS_USERNAME: admin
        OS_USER_DOMAIN_NAME: "default"
        OS_PROJECT_DOMAIN_NAME: "default"
      run_once: True
      retries: 10
      delay: 5

    - name: coriolis-api container
      docker_container:
        name: coriolis-api
        image: "{{ docker_registry }}/{{ coriolis_containers_namespace }}/coriolis-api:{{ docker_images_tag }}"
        pull: yes
        hostname: coriolis-api
        state: started
        restart: yes
        restart_policy: unless-stopped
        network_mode: host
        volumes:
        - "{{ coriolis_config_directory_path }}:{{ coriolis_config_directory_path }}:ro"
        - "/var/log/coriolis:/var/log/coriolis:rw"
        ports:
        - "7667:7667"

    - name: coriolis-conductor container
      docker_container:
        name: coriolis-conductor
        image: "{{ docker_registry }}/{{ coriolis_containers_namespace }}/coriolis-conductor:{{ docker_images_tag }}"
        pull: yes
        hostname: coriolis-conductor
        state: started
        restart: yes
        restart_policy: unless-stopped
        network_mode: host
        volumes:
        - "{{ coriolis_config_directory_path }}:{{ coriolis_config_directory_path }}:ro"
        - "/var/log/coriolis:/var/log/coriolis:rw"

    - name: coriolis-replica-cron container
      docker_container:
        name: coriolis-replica-cron
        image: "{{ docker_registry }}/{{ coriolis_containers_namespace }}/coriolis-replica-cron:{{ docker_images_tag }}"
        pull: yes
        hostname: coriolis-replica-cron
        state: started
        restart: yes
        restart_policy: unless-stopped
        network_mode: host
        volumes:
        - "{{ coriolis_config_directory_path }}:{{ coriolis_config_directory_path }}:ro"
        - "/var/log/coriolis:/var/log/coriolis:rw"

    - name: coriolis-web container
      docker_container:
        name: coriolis-web
        image: "{{ docker_registry }}/{{ coriolis_containers_namespace }}/coriolis-web:{{ docker_images_tag }}"
        pull: yes
        hostname: coriolis-web
        state: started
        restart: yes
        restart_policy: unless-stopped
        network_mode: host

    - name: coriolis-web-proxy container
      docker_container:
        name: coriolis-web-proxy
        image: "{{ docker_registry }}/{{ coriolis_containers_namespace }}/coriolis-web-proxy:{{ docker_images_tag }}"
        pull: yes
        hostname: coriolis-web-proxy
        state: started
        restart: yes
        restart_policy: unless-stopped
        network_mode: host
        env:
          KEYSTONE_AUTH_URL: "{{ keystone_auth_url_v3 }}"
          BARBICAN_ENDPOINT_URL: "{{ barbican_endpoint_url }}"
          CORIOLIS_BASE_ENDPOINT_URL: "{{ coriolis_base_endpoint_url }}"
          CORIOLIS_INTERNAL_URL: "{{ coriolis_internal_url }}"
